package com.rokas.dontforget;

import android.view.ViewConfiguration;

import com.orm.SugarApp;

import java.lang.reflect.Field;

/**
 * Created by rokas on 14.7.26.
 */
public class CustomApplication extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
