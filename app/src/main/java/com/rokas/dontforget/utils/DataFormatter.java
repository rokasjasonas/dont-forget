package com.rokas.dontforget.utils;

import android.content.Context;
import android.util.Log;

import com.rokas.dontforget.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by rokas on 14.7.24.
 */
public class DataFormatter {
    private static SimpleDateFormat dateFormat;

    public static String formatDate(String originalDate, Context context) {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm", Locale.US);
        }

//        try {
//            final Calendar betDate = Calendar.getInstance();
//            betDate.setTime(dateFormat.parse(originalDate));
//            final Calendar currentDate = Calendar.getInstance();
//            String weekday = null;
//            switch (betDate.get(Calendar.DAY_OF_WEEK)) {
//                case Calendar.MONDAY:
//                    weekday = context.getString(R.string.monday);
//                    break;
//                case Calendar.TUESDAY:
//                    weekday = context.getString(R.string.tuesday);
//                    break;
//                case Calendar.WEDNESDAY:
//                    weekday = context.getString(R.string.wednesday);
//                    break;
//                case Calendar.THURSDAY:
//                    weekday = context.getString(R.string.thursday);
//                    break;
//                case Calendar.FRIDAY:
//                    weekday = context.getString(R.string.friday);
//                    break;
//                case Calendar.SATURDAY:
//                    weekday = context.getString(R.string.saturday);
//                    break;
//                case Calendar.SUNDAY:
//                    weekday = context.getString(R.string.sunday);
//                    break;
//            }
//            if (betDate.get(Calendar.YEAR) != currentDate.get(Calendar.YEAR)) {
//                return originalDate + ", " + weekday;
//            }
//
//            if (betDate.get(Calendar.MONTH) == currentDate.get(Calendar.MONTH)) {
//                if (betDate.get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH) + 1) { // rytoj
//                    return context.getString(R.string.tomorrow) + " " + numberWithZero(betDate.get(Calendar.HOUR_OF_DAY)) + ":"
//                            + numberWithZero(betDate.get(Calendar.MINUTE));
//                } else if (betDate.get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH)) { // siandien
//                    if (betDate.get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH)) {
//                        final long timeLeft = betDate.getTimeInMillis() - currentDate.getTimeInMillis();
//
//                        if (timeLeft <= 60 * 60 * 1000 && timeLeft > 0) { // jei greiciau nei 60min
//                            return context.getString(R.string.after) + " " +
//                                    timeLeft / 60000 + " " + context.getString(R.string.min_abb);
//                        } else if (timeLeft <= 0) {
//                            return context.getString(R.string.bid_time_expired);
//                        } else {
//                            return context.getString(R.string.today) + " " +
//                                    numberWithZero(betDate.get(Calendar.HOUR_OF_DAY)) + ":" +
//                                    numberWithZero(betDate.get(Calendar.MINUTE));
//                        }
//                    }
//                }
//            }
//            return numberWithZero(betDate.get(Calendar.MONTH) + 1) + "-" +
//                    numberWithZero(betDate.get(Calendar.DAY_OF_MONTH)) + " " +
//                    numberWithZero(betDate.get(Calendar.HOUR_OF_DAY)) + ":" +
//                    numberWithZero(betDate.get(Calendar.MINUTE)) + ", " + weekday;
//        } catch (Exception e) {
//            Log.e("TAG", "Error: " + e);
            return originalDate;
//        }
    }

    public static String numberWithZero(int number) {
        if (number < 10) {
            return "0" + number;
        }
        return number + "";
    }
}
