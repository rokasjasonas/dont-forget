package com.rokas.dontforget.ui.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.rokas.dontforget.R;
import com.rokas.dontforget.models.DontForgetItem;
import com.rokas.dontforget.ui.fragments.ListFragment;

import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private static final long UPDATE_WAIT_TIME = 500;
    private final int transparent;
    private ListFragment listFragment;
    private List<DontForgetItem> mDataset;
    private int red;

    // Provide a reference to the type of views that you are using
    // (custom viewholder)
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public Spinner spLimit;
        public TextView tvTime;
        public ImageButton ibReset;
        public ImageButton ibEdit;
        public View parentView;
        public Handler handler;

        public ViewHolder(View v) {
            super(v);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
            spLimit = (Spinner) v.findViewById(R.id.sp_time_limit);
            ibReset = (ImageButton) v.findViewById(R.id.ib_reset);
            ibEdit = (ImageButton) v.findViewById(R.id.ib_edit);
            parentView = v;
            handler = new Handler();
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListAdapter(ListFragment listFragment, List<DontForgetItem> myDataset) {
        this.listFragment = listFragment;
        mDataset = myDataset;
        red = listFragment.getActivity().getResources().getColor(R.color.red);
        transparent = listFragment.getActivity().getResources().getColor(R.color.transparent);
    }

    public void add(DontForgetItem dontForgetItem) {
        if (dontForgetItem != null) {
            mDataset.add(dontForgetItem);
        }
    }

    public void remove(DontForgetItem dontForgetItem) {
        mDataset.remove(dontForgetItem);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final DontForgetItem dontForgetItem = mDataset.get(position);

        updateText(holder, dontForgetItem);
        updateTime(holder, dontForgetItem);
        updateLimit(holder, dontForgetItem);

        holder.handler.removeCallbacksAndMessages(null);

        holder.handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateTime(holder, dontForgetItem);
                holder.handler.postDelayed(this, UPDATE_WAIT_TIME);
            }
        }, UPDATE_WAIT_TIME);

        holder.ibReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResetDialog(holder, dontForgetItem);
            }
        });
        holder.ibEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listFragment.showEditItemDialog(holder, dontForgetItem);
            }
        });
    }

    private void updateLimit(ViewHolder holder, DontForgetItem dontForgetItem) {
        if (dontForgetItem != null && dontForgetItem.getLimit() != 0 && System.currentTimeMillis() > dontForgetItem.getStarted() + dontForgetItem.getLimit()) {
            holder.parentView.setBackgroundColor(red);
        } else {
            holder.parentView.setBackgroundColor(transparent);
        }
    }

    private void updateText(ViewHolder holder, DontForgetItem dontForgetItem) {
        if (dontForgetItem != null) {
            holder.tvTitle.setText(dontForgetItem.getTitle());
        }
    }

    private void showResetDialog(final ViewHolder holder, final DontForgetItem dontForgetItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(holder.ibReset.getContext());
        builder.setMessage(R.string.do_you_want_to_reset)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dontForgetItem.setStarted(System.currentTimeMillis());
                        updateDontForgetItem(holder, dontForgetItem);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateDontForgetItem(ViewHolder holder, DontForgetItem dontForgetItem) {
        dontForgetItem.save();
        int location = mDataset.indexOf(dontForgetItem);
        mDataset.set(location, dontForgetItem);
        updateText(holder, dontForgetItem);
        updateTime(holder, dontForgetItem);
        updateLimit(holder, dontForgetItem);
    }

    private void updateTime(ViewHolder holder, DontForgetItem dontForgetItem) {
        if (dontForgetItem != null) {
            Context context = holder.tvTime.getContext();
            holder.tvTime.setText(calculateTime(context, dontForgetItem.getStarted()));
        }
    }

    private String calculateTime(Context context, long started) {
        String answer = "";
        boolean isBeginingHiden = false;

        long diffLeft = System.currentTimeMillis();
        long diff = new Period(started, diffLeft, PeriodType.years()).getYears();
        if (diff > 0) {
            answer += diff + " " + context.getString(R.string.years) + " ";
            isBeginingHiden = true;
        }
        diffLeft -= diff * 12 * 31 * 24 * 60 * 60 * 1000;
        diff = new Period(started, diffLeft, PeriodType.months()).getMonths();
        if (diff > 0) {
            if (!isBeginingHiden) {
                answer += diff + " " + context.getString(R.string.months) + " ";
            }
        }

        diffLeft -= diff * 31 * 24 * 60 * 60 * 1000;
        diff = new Period(started, diffLeft, PeriodType.days()).getDays();
        if (diff > 0) {
            if (!isBeginingHiden) {
                answer += diff + " " + context.getString(R.string.days) + " ";
            }
        }

        diffLeft -= diff * 24 * 60 * 60 * 1000;
        diff = new Period(started, diffLeft, PeriodType.hours()).getHours();
        if (diff > 0) {
            if (!isBeginingHiden) {
                answer += diff + context.getString(R.string.hours) + " ";
            }
        }

        diffLeft -= diff * 60 * 60 * 1000;
        diff = new Period(started, diffLeft, PeriodType.minutes()).getMinutes();
        if (diff > 0) {
            if (!isBeginingHiden) {
                answer += diff + context.getString(R.string.minutes) + " ";
            }
        }

        diffLeft -= diff * 60 * 1000;
        diff = new Period(started, diffLeft, PeriodType.seconds()).getSeconds();
        if (diff > 0) {
            if (!isBeginingHiden) {
                answer += diff + context.getString(R.string.seconds) + " ";
            }
        }

        return answer;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}