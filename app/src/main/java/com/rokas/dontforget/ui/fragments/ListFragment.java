package com.rokas.dontforget.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.rokas.dontforget.R;
import com.rokas.dontforget.models.DontForgetItem;
import com.rokas.dontforget.ui.adapters.ListAdapter;
import com.rokas.dontforget.ui.base.BaseFragment;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ListFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";

    private ListAdapter adapter;
    private Calendar calendar; // from picker
    private TextView tvTempTime;
    private final static String SIMPLE_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";


    public ListFragment() {
    }

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        RecyclerView rvList = (RecyclerView) rootView.findViewById(R.id.rv_list);
        rvList.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvList.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        List<DontForgetItem> dontForgetItems = DontForgetItem.listAll(DontForgetItem.class);

        adapter = new ListAdapter(this, dontForgetItems);
        rvList.setAdapter(adapter);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showAddItemDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddItemDialog() {
        showEditItemDialog(null, null);
    }

    public void showEditItemDialog(final ListAdapter.ViewHolder holder, final DontForgetItem dontForgetItem) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View inflate = inflater.inflate(R.layout.dialog_edit_item, null);
        final EditText etTitle = (EditText) inflate.findViewById(R.id.et_title);
        final Button tvTime = (Button) inflate.findViewById(R.id.tv_time);
        final Spinner spLimit = (Spinner) inflate.findViewById(R.id.sp_time_limit);

        calendar = Calendar.getInstance();

        final String[] timeLimitTitles = getResources().getStringArray(R.array.time_limit_titles);
        final String[] timeLimitValues = getResources().getStringArray(R.array.time_limit_values);

        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, timeLimitTitles);

        spLimit.setAdapter(spinnerAdapter);

        if (dontForgetItem != null) {
            etTitle.setText(dontForgetItem.getTitle());
            calendar.setTimeInMillis(dontForgetItem.getStarted());

            final int position = Arrays.binarySearch(timeLimitValues, String.valueOf(dontForgetItem.getLimit()));
            if (position >= 0) {
                spLimit.setSelection(position);
            }
        } else {
            calendar.setTimeInMillis(System.currentTimeMillis());
        }

        final String date = new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(calendar.getTimeInMillis());
        tvTime.setText(date);

        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimePicker(calendar, tvTime);
            }
        });

        builder.setView(inflate)
                // Add action buttons
                .setPositiveButton(dontForgetItem != null ? R.string.edit : R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        if (holder != null && dontForgetItem != null) {
            builder.setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showDeleteDialog(holder, dontForgetItem);
                }
            });
        }

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String title = etTitle.getText().toString();
                final long time = calendar.getTimeInMillis();
                final long limit = Long.valueOf(timeLimitValues[spLimit.getSelectedItemPosition()]);

                if (dontForgetItem == null && DontForgetItem.find(DontForgetItem.class, "title = ?", title).size() >= 1) {
                    Toast.makeText(getActivity(), R.string.item_with_title_already_exists, Toast.LENGTH_SHORT).show();
                } else {
                    if (holder != null) {
                        dontForgetItem.setTitle(title);
                        dontForgetItem.setStarted(time);
                        dontForgetItem.setLimit(limit);

                        adapter.updateDontForgetItem(holder, dontForgetItem);
                    } else {
                        final DontForgetItem dontForgetItem = new DontForgetItem(title, time, limit);
                        dontForgetItem.save();
                        adapter.add(dontForgetItem);
                    }
                    alertDialog.dismiss();
                }
            }
        });

    }

    public void showDeleteDialog(final ListAdapter.ViewHolder viewHolder, final DontForgetItem dontForgetItem) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(viewHolder.ibReset.getContext());
        builder.setMessage(R.string.do_you_want_to_delete)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        adapter.remove(dontForgetItem);
                        dontForgetItem.delete();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showDateTimePicker(Calendar calendar, TextView tvTime) {
        tvTempTime = tvTime;
        final DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        datePickerDialog.setVibrate(false);
        datePickerDialog.setYearRange(1992, 2028);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getFragmentManager(), DATEPICKER_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i2, int i3) {
        calendar.set(i, i2, i3);
        final TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE), true, false);
        timePickerDialog.setVibrate(false);
        timePickerDialog.setCloseOnSingleTapMinute(false);
        timePickerDialog.show(getFragmentManager(), TIMEPICKER_TAG);
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i2) {
        calendar.set(Calendar.HOUR_OF_DAY, i);
        calendar.set(Calendar.MINUTE, i2);
        calendar.set(Calendar.SECOND, 0);

        if (tvTempTime != null) {
            final String date = new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(calendar.getTimeInMillis());
            tvTempTime.setText(date);
        }
    }
}