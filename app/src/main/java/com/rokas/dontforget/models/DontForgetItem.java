package com.rokas.dontforget.models;

import com.orm.SugarRecord;

/**
 * Created by rokas on 14.7.24.
 */
public class DontForgetItem extends SugarRecord<DontForgetItem> {
    private String title;
    private long started;
    private long timeLimit;

    public DontForgetItem() {
    }

    public DontForgetItem(String title, long started, long timeLimit) {
        this.title = title;
        this.started = started;
        this.timeLimit = timeLimit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStarted() {
        return started;
    }

    public void setStarted(long started) {
        this.started = started;
    }

    @Override
    public boolean equals(Object o) {
        if (this.getId().equals(((DontForgetItem) o).getId())) {
            return true;
        }
        return super.equals(o);
    }

    public long getLimit() {
        return timeLimit;
    }


    public void setLimit(long timeLimit) {
        this.timeLimit = timeLimit;
    }
}
